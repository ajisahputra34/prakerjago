FROM golang:latest

WORKDIR /go/src/app

COPY . .

# RUN go mod download github.com/stretchr/objx

# RUN go build -o main

CMD ["go","run","main.go"]
# CMD ["./main"]